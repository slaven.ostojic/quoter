FROM gradle:8.10.1-jdk21-alpine AS builder
WORKDIR /app/src
COPY . .
RUN ./gradlew build

FROM openjdk:21
COPY --from=builder /app/src/build/libs/*SNAPSHOT.jar /app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]
