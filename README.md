# Quoter

<a href="https://gitlab.com/slaven.ostojic/quoter" target="_blank">
    <img src="quoter.png" alt="Logo" width="160"/>
</a>

> Quote getter based on ChatGPT and RAG.

- Java 21
- Spring Boot
- Spring AI
- PostgreSQL and PGvector
- OpenAPI and Swagger
- Kubernetes (minikube and kubectl)
- Docker
- Gradle

### Endpoints

To initialize data:

```http request
POST localhost:8080/quote
```

To get random quote:

```http request
GET localhost:8080/quote
```

To ask anything:

```http request
POST localhost:8080/chat

Tell me a joke
```

To ask anything and get reply with quote:

```http request
POST localhost:8080/chat/quote

What to write on post card for a wedding?
```

### API Specification

API specification is available on:
- OpenAPI 3: http://localhost:8080/v3/api-docs
- Swagger: http://localhost:8080/swagger-ui/index.html

### Configuration

`spring.ai.openai.api-key` should be valid OpenAI API key. OpenAI does not support free tier API keys for API
interaction.

### Storage

`resources/quotes.json` keep all quotes.

To run a database, use `docker compose up`.

These quotes will be loaded into PostgreSQL table.

Embeddings created via OpenAI will be loaded into PGvector vector store.

### Build, run and stop

```shell
docker build -t quoter .
docker run -p 8080:8080 --name quoter quoter
docker stop quoter
```

### Run minikube

```shell
minikube start
minikube status
eval $(minikube -p minikube docker-env)
docker build -t quoter .   # build docker image so it can be available in minikube registry
minikube dashboard
```

### Deploy

```shell
./k8s-apply-all.sh
```

### Introspection

```shell
kubectl get deployments
kubectl get pods
kubectl logs <pod_name>
kubectl get service
```

### Access

Next command should return URL and port where the application is accessible. However, on Darwin architecture, tunnel
should be created.

```shell
kubectl get nodes -o wide
minikube ip
minikube service quoter
```

The application is now accessible on `http://<host>:<port>/quote`.

> `<host>` and `<port>` will be resolved after the last command.

### Delete

```shell
./k8s-delete-all.sh
```

### Stop

```shell
eval $(minikube -p minikube docker-env --unset)
minikube stop
```
