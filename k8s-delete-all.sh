#!/bin/bash

kubectl delete -f k8s/quoter-service.yml
kubectl delete -f k8s/quoter-deployment.yml
kubectl delete -f k8s/postgres-service.yml
kubectl delete -f k8s/postgres-deployment.yml
kubectl delete -f k8s/postgres-pvc.yml
kubectl delete -f k8s/postgres-secret.yml
