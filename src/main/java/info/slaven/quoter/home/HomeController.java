package info.slaven.quoter.home;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
@Tag(name = "home", description = "Quoter related endpoints")
public class HomeController {
    @GetMapping
    public String home() {
        return """
                Quoter.
                API specification available on: /v3/api-docs and /swagger-ui/index.html
                """;
    }
}
