package info.slaven.quoter.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.SpecVersion;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI(SpecVersion.V31)
                .info(new Info()
                        .title("Quoter API")
                        .description("API for interaction with Quoter")
                        .summary("API Specification")
                        .version("0.0.1-SNAPSHOT")
                        .contact(new Contact()
                                .name("Slaven Ostojic")
                                .url("https://slaven.info")));
    }
}
