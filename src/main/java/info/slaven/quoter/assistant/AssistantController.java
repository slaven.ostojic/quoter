package info.slaven.quoter.assistant;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chat")
@RequiredArgsConstructor
@Tag(name = "assistant", description = "AI assistant related endpoints")
public class AssistantController {

    private final ChatAssistant chatAssistant;

    @Operation(summary = "Ask a question", description = "Returns an answer to the question")
    @PostMapping
    String ask(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Question to ask") @RequestBody String question) {
        return chatAssistant.ask(question);
    }

    @Operation(summary = "Ask a question", description = "Returns an answer with additional quote to the question")
    @PostMapping("/quote")
    String askForQuote(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Question to ask") @RequestBody String question) {
        return chatAssistant.askForQuote(question);
    }
}
