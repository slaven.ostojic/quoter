package info.slaven.quoter.assistant;

import info.slaven.quoter.quote.QuoteStorage;
import org.springframework.ai.chat.client.ChatClient;
import org.springframework.ai.chat.client.advisor.MessageChatMemoryAdvisor;
import org.springframework.ai.chat.memory.InMemoryChatMemory;
import org.springframework.stereotype.Service;

@Service
public class ChatAssistant {

    private final ChatClient chatClient;
    private final QuoteStorage quoteStorage;

    ChatAssistant(ChatClient.Builder chatClientBuilder, QuoteStorage quoteStorage) {
        chatClient = chatClientBuilder
                .defaultAdvisors(new MessageChatMemoryAdvisor(new InMemoryChatMemory()))
                .build();
        this.quoteStorage = quoteStorage;
    }

    public String ask(String question) {
        return chatClient.prompt()
                .user(question)
                .call()
                .content();
    }

    public String askForQuote(String question) {
        var similar = quoteStorage.getSimilar(question);
        var system = """
                You should answer the user question. Question should contain a quote describing the answer.
                
                Use the information from the DOCUMENTS section to provide quotes but act as if you knew this quote already.
                If unsure, simply state that you don't know.
                
                DOCUMENTS:
                {documents}
                """;

        return chatClient.prompt()
                .user(question)
                .system(promptSystemSpec -> promptSystemSpec.text(system).param("documents", similar))
                .call()
                .content();
    }

}
