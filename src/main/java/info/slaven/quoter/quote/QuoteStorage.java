package info.slaven.quoter.quote;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ai.document.Document;
import org.springframework.ai.reader.JsonReader;
import org.springframework.ai.vectorstore.VectorStore;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuoteStorage {

    private final VectorStore vectorStore;
    private final QuoteRepository quoteRepository;

    @Transactional
    public List<Data> initialize() {
        var resource = new ClassPathResource("quotes.json");
        var quotes = load(resource);
        var entities = quotes.stream().map(data -> {
            var entity = new QuoteEntity();
            entity.setAuthor(data.author());
            entity.setValue(data.quote());
            return entity;
        }).toList();
        quoteRepository.saveAll(entities);
        log.info("{} quotes stored.", entities.size());

        var reader = new JsonReader(resource);
        var documents = reader.get();
        vectorStore.add(documents);
        log.info("Embeddings added.");

        return quotes;
    }

    @Transactional(readOnly = true)
    public Data getAny() {
        var entity = quoteRepository.findAny();
        log.info("Entity found: {}", entity);
        if (entity == null) {
            return new Data("(N/A)", "(N/A)");
        }
        return new Data(entity.getValue(), entity.getAuthor());
    }

    public String getSimilar(String query) {
        var documents = vectorStore.similaritySearch(query);
        var result = documents.stream().map(Document::getContent).collect(Collectors.joining(System.lineSeparator()));
        log.debug("Vector store similarity result: {}", result);
        return result;
    }

    private List<Data> load(ClassPathResource resource) {
        try {
            var mapper = new ObjectMapper();
            var content = resource.getContentAsString(StandardCharsets.UTF_8);
            var data = mapper.readValue(content, Data[].class);
            return Arrays.stream(data).toList();
        } catch (Exception e) {
            log.error("Cannot process 'quotes.json`");
            return Collections.emptyList();
        }
    }
}
