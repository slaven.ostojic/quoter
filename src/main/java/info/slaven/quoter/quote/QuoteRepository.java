package info.slaven.quoter.quote;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface QuoteRepository extends JpaRepository<QuoteEntity, Long> {

    @Query(value = "SELECT e FROM QuoteEntity e ORDER BY RANDOM() LIMIT 1")
    QuoteEntity findAny();

}
