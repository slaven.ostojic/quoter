package info.slaven.quoter.quote;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/quote")
@Tag(name = "quote", description = "Quote related endpoints")
public class QuoteController {

    private final QuoteStorage quoteStorage;

    public QuoteController(QuoteStorage quoteStorage) {
        this.quoteStorage = quoteStorage;
    }

    @Operation(summary = "Get random quote", description = "Returns a single quote if any exists")
    @GetMapping
    public String getQuote() {
        log.debug("Getting a quote...");
        var data = quoteStorage.getAny();
        return "'" + data.quote() + "' - " + data.author();
    }

    @Operation(summary = "Initialize quotes", description = "Loads quotes into the system")
    @PostMapping
    public List<Data> initialize() {
        return quoteStorage.initialize();
    }

}
