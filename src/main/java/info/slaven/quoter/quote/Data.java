package info.slaven.quoter.quote;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Quote model")
public record Data(@Schema(description = "The quote") String quote,
                   @Schema(description = "Quote author") String author) {
}
